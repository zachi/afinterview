# AF Interview Project

## Setup
- Part2 of the project is on `Assets/Scenes/BattleScene.unity` scene 
- All unit's balance can be done by changing values within `Assets/Prefabs/BattleGame/ArmyUnitDefinitions.asset`
- Both armies are placed on `BattleScene` - to change units within army change the `Armies/LeftArmy` or `Armies/RightArmy` object

## Gameplay
- Unit with _green head_ is the active unit
- Whose with _pink heads_ are possible targets
- You choose target by clicking it

## Use od design patterns:
- `BattleManager` is basic Singleton
- For game flow I used `StateMachine` - implemented in `BattleStateMachine` class, with state implementing `IBattleStateBase` interface
- `ArmyUnitInstance` is implementation of `Flyweight` pattern. It doesn't really saves much memory in this specific case, but helps separate mutable (currentHP, turnsToAttack) fields from immutable unit definition
- `ArmyUnitPresenter` **was** `Observer` of `ArmyUnitInstance` for most part of development, but when I added _"animations"_ I _broke_ this pattern a little. Let's say it's partial-observer now ;)
-  Still - it separates Presentation from Data part of ArmyUnit, so I would say it's `Model-View` connection    

## Last words
- Of course I would use DOTween (or whatever you're using in Ancient Forge) to write unit movement, but want to keep it small and _vanilla_