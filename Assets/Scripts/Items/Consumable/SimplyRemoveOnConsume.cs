using UnityEngine;
namespace AFSInterview.Items.Consumable
{
    [CreateAssetMenu(menuName = MENU_NAME + nameof(SimplyRemoveOnConsume), fileName = nameof(SimplyRemoveOnConsume))]
    public class SimplyRemoveOnConsume : ConsumableBase
    {
        public override bool HandleConsume(InventoryController controller)
        {
            return true;
        }
    }
}