using UnityEngine;
namespace AFSInterview.Items.Consumable
{
    public abstract class ConsumableBase : ScriptableObject
    {
        public const string MENU_NAME = "Items/Consumable/";
        public abstract bool HandleConsume(InventoryController controller);
    }
}