using UnityEngine;
namespace AFSInterview.Items.Consumable
{
    [CreateAssetMenu(menuName = MENU_NAME + nameof(WriteDebugOnConsumeButDontReallyConsume), fileName = nameof(WriteDebugOnConsumeButDontReallyConsume))]
    public class WriteDebugOnConsumeButDontReallyConsume : ConsumableBase
    {
        public override bool HandleConsume(InventoryController controller)
        {
            Debug.Log("Hello there!");
            return false;
        }
    }
}