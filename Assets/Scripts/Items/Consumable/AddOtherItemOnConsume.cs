using UnityEngine;
namespace AFSInterview.Items.Consumable
{
    [CreateAssetMenu(menuName = MENU_NAME + nameof(AddOtherItemOnConsume), fileName = nameof(AddOtherItemOnConsume))]
    public class AddOtherItemOnConsume : ConsumableBase
    {
        [SerializeField] private Item itemToAdd;
        
        public override bool HandleConsume(InventoryController controller)
        {
            controller.AddItem(itemToAdd);
            return true;
        }
    }
}