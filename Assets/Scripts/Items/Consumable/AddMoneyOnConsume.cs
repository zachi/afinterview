using UnityEngine;
namespace AFSInterview.Items.Consumable
{
    [CreateAssetMenu(menuName = MENU_NAME + nameof(AddMoneyOnConsume), fileName = nameof(AddMoneyOnConsume))]
    public class AddMoneyOnConsume : ConsumableBase
    {
        [SerializeField] private int moneyToAdd;

        public override bool HandleConsume(InventoryController controller)
        {
            controller.Money += moneyToAdd;
            return true;
        }
    }
}