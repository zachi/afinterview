﻿namespace AFSInterview.Items
{
	using TMPro;
	using UnityEngine;

	public class ItemsManager : MonoBehaviour
	{
		[SerializeField] private InventoryController inventoryController;
		[SerializeField] private int itemSellMaxValue;
		[SerializeField] private Transform itemSpawnParent;
		[SerializeField] private ItemPresenter[] itemPrefabs;
		[SerializeField] private BoxCollider itemSpawnArea;
		[SerializeField] private float itemSpawnInterval;
		
		[SerializeField] private TMP_Text cachedMoneyLabel;
		#if DBG_TEST_USE_ITEM
		[SerializeField] private int inventoryItemIndex;
		#endif

		private float nextItemSpawnTime;

		private Camera cachedMainCamera;
		private int cachedLayerMask;
		private readonly RaycastHit[] rcHit = new RaycastHit[1];

		private void Start()
		{
			cachedMainCamera = Camera.main;
			cachedLayerMask = LayerMask.GetMask("Item");
			cachedMoneyLabel = FindObjectOfType<TextMeshProUGUI>();
		}

		private void OnEnable()
		{
			inventoryController.MoneyChanged += OnMoneyChanged;
			OnMoneyChanged(inventoryController.Money);
		}

		private void OnDisable()
		{
			inventoryController.MoneyChanged -= OnMoneyChanged;
		}
		
		private void Update()
		{
			if (Time.time >= nextItemSpawnTime)
				SpawnNewItem();
			
			if (Input.GetMouseButtonDown(0))
				TryPickUpItem();
			
			if (Input.GetKeyDown(KeyCode.Space))
				inventoryController.SellAllItemsUpToValue(itemSellMaxValue);
			
			#if DBG_TEST_USE_ITEM
			if (Input.GetKeyDown(KeyCode.C))
				UseItem(inventoryController.ItemAt[inventoryItemIndex]);
			#endif
		}

		private void OnMoneyChanged(int money)
		{
			cachedMoneyLabel.text = "Money: " + inventoryController.Money;
		}

		private void SpawnNewItem()
		{
			nextItemSpawnTime = Time.time + itemSpawnInterval;
			
			var spawnAreaBounds = itemSpawnArea.bounds;
			var position = new Vector3(
				Random.Range(spawnAreaBounds.min.x, spawnAreaBounds.max.x),
				0f,
				Random.Range(spawnAreaBounds.min.z, spawnAreaBounds.max.z)
			);
			
			Instantiate(itemPrefabs[Random.Range(0, itemPrefabs.Length)], position, Quaternion.identity, itemSpawnParent);
		}

		private void TryPickUpItem()
		{
			var ray = cachedMainCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.RaycastNonAlloc(ray, rcHit, 100f, cachedLayerMask) < 1 || !rcHit[0].collider.TryGetComponent<IItemHolder>(out var itemHolder))
				return;
			
			var item = itemHolder.GetItem(true);
            inventoryController.AddItem(item);
            Debug.Log("Picked up " + item.Name + " with value of " + item.Value + " and now have " + inventoryController.ItemsCount + " items");
		}

		private void UseItem(Item i)
		{
			if (i.Use(inventoryController))
				inventoryController.RemoveItem(i);
		}
	}
}