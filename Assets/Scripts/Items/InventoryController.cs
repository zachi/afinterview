﻿using System;
namespace AFSInterview.Items
{
    using System.Collections.Generic;
    using UnityEngine;

    public class InventoryController : MonoBehaviour
    {
        public event Action<int> MoneyChanged;

        [SerializeField] private List<Item> items;
        [SerializeField] private int money;

        public IReadOnlyList<Item> ItemAt => items;

        public int Money
        {
            get => money;
            set
            {
                money = value;
                MoneyChanged?.Invoke(money);
            }
        }

        public int ItemsCount => items.Count;

        public void SellAllItemsUpToValue(int maxValue)
        {
            for (int i = items.Count - 1; i >= 0; i--)
            {
                var itemValue = items[i].Value;
                if (itemValue > maxValue)
                    continue;

                Money += itemValue;
                items.RemoveAt(i);
            }
        }
        
        public void AddItem(Item item)
        {
            items.Add(item);
        }
        
        public void RemoveItem(Item item)
        {
            items.Remove(item);
        }
    }
}