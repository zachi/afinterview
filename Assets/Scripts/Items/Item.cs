﻿using AFSInterview.Items.Consumable;
namespace AFSInterview.Items
{
	using System;
	using UnityEngine;

	[Serializable]
	public class Item
	{
		[SerializeField] private string name;
		[SerializeField] private int value;
		[SerializeField] private ConsumableBase consumableImplementation;

		public string Name => name;
		public int Value => value;

		public Item(string name, int value)
		{
			this.name = name;
			this.value = value;
		}

		public bool Use(InventoryController inventory)
		{
			Debug.Log("Using" + Name);
			return consumableImplementation && consumableImplementation.HandleConsume(inventory);
		}
	}
}