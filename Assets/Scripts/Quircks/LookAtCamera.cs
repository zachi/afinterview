using UnityEngine;
namespace AFSInterview.Quircks
{
    public class LookAtCamera : MonoBehaviour
    {
        private void Start()
        {
            transform.rotation = Camera.main.transform.rotation;
        }
    }
}