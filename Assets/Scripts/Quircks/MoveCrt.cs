using System.Collections;
using UnityEngine;

namespace AFSInterview.Quircks
{
    public static class MoveCrt
    {
        public static IEnumerator MoveFromTo(Transform objectToMove, Vector3 pointA, Vector3 pointB, float duration)
        {
            float elapsedTime = 0;
            while (elapsedTime < duration)
            {
                float t = elapsedTime / duration;
                t = Mathf.SmoothStep(0.0f, 1.0f, t);
                objectToMove.position = Vector3.Lerp(pointA, pointB, t);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            objectToMove.position = pointB;
        }
    }
}