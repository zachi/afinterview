namespace AFSInterview.Items.Battle
{
    public class AwaitUnitActionState : IBattleStateBase
    {
        private readonly ArmyUnitInstance forUnit;
        
        public AwaitUnitActionState(ArmyUnitInstance forUnit)
        {
            this.forUnit = forUnit;
        }
        
        public void HandleEnterState()
        {
            BattleManager.Instance.MarkUnit(forUnit, true);
            BattleManager.Instance.MarkPossibleTargets(forUnit, true);
        }
        
        public void HandleExitState()
        {
            BattleManager.Instance.MarkUnit(forUnit, false);
            BattleManager.Instance.MarkPossibleTargets(forUnit, false);
        }
        
        public void ProcessChoice(ArmyUnitInstance unitInstance)
        {
            var damage = DamageResolver.CalculateTrueDamage(forUnit.Definition, unitInstance.Definition);
            BattleManager.Instance.PlayAttack(forUnit, unitInstance, damage);
            unitInstance.TakeDamage(damage);
            forUnit.ResetTurnsCounter();
        }
    }
}