using AFSInterview.Items.Battle.Presentation;
namespace AFSInterview.Items.Battle
{
    public interface IBattleStateBase
    {
        public void HandleEnterState();
        public void HandleExitState();
        public void ProcessChoice(ArmyUnitInstance unitInstance);
        public bool IsUnitValidForState(ArmyUnitPresenter unitPresenter) => unitPresenter.IsMarkedAsPossibleTarget;
    }
}