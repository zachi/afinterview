using AFSInterview.Items.Battle.Presentation;
using UnityEngine;
namespace AFSInterview.Items.Battle
{
    public class BattleStateMachine : MonoBehaviour
    {
        private Camera cachedMainCamera;
        private int cachedLayerMask;
        private readonly RaycastHit[] rcHit = new RaycastHit[1];

        private void Start()
        {
            cachedMainCamera = Camera.main;
            cachedLayerMask = LayerMask.GetMask("Item");
        }

        [SerializeField] private GameOverState gameOverState;
        [SerializeField] private PerformAnimationsState animationsState;

        private IBattleStateBase CurrentState;

        public bool EnterStateForUnitAction(ArmyUnitInstance unitInstance)
        {
            if (unitInstance.IsAlive() && unitInstance.WouldProcessTurn())
            {
                var state = new AwaitUnitActionState(unitInstance);
                SwitchStateTo(state);
                return true;
            }

            return false;
        }

        public void PlayAnimAttack(ArmyUnitPresenter from, ArmyUnitPresenter to, int damage)
        {
            animationsState.Setup(from, to, damage);
            SwitchStateTo(animationsState);
        }

        public void GameOver() => SwitchStateTo(gameOverState);

        private void SwitchStateTo(IBattleStateBase newState)
        {
            CurrentState?.HandleExitState();
            CurrentState = newState;
            newState?.HandleEnterState();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = cachedMainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.RaycastNonAlloc(ray, rcHit, 100f, cachedLayerMask) < 1 || !rcHit[0].collider.TryGetComponent<ArmyUnitPresenter>(out var unitPresenter))
                    return;

                if (CurrentState.IsUnitValidForState(unitPresenter))
                {
                    CurrentState.ProcessChoice(unitPresenter.UnitInstance);
                }
            }
        }
    }
}