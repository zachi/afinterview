using UnityEngine;

namespace AFSInterview.Items.Battle
{
    public class GameOverState : MonoBehaviour, IBattleStateBase
    {
        [SerializeField] private GameObject gameOverScreen;
        
        public void HandleEnterState()
        {
            gameOverScreen.SetActive(true);
        }
        
        public void HandleExitState() { }
        
        public void ProcessChoice(ArmyUnitInstance unitInstance) { }
    }
}