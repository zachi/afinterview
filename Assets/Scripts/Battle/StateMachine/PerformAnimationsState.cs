using System.Collections;
using AFSInterview.Items.Battle.Presentation;
using AFSInterview.Quircks;
using UnityEngine;

namespace AFSInterview.Items.Battle
{
    public class PerformAnimationsState : MonoBehaviour, IBattleStateBase
    {
        [SerializeField] private GameObject hitSpawn;
        [SerializeField] private DamageLabel damageLabel;
        
        private ArmyUnitPresenter attacker;
        private ArmyUnitPresenter defender;
        private int damage;

        public void Setup(ArmyUnitPresenter attacking, ArmyUnitPresenter dafending, int attackDmg)
        {
            attacker = attacking;
            defender = dafending;
            damage = attackDmg;
        }

        public void HandleEnterState()
        {
            StartCoroutine(PerformAnimations());

            IEnumerator PerformAnimations()
            {
                var transform1 = attacker.transform;
                var targetPosition = defender.transform.position;
                var attackerPosition = transform1.position;
                
                yield return MoveCrt.MoveFromTo(transform1, attackerPosition, targetPosition, .15f);
                defender.UpdateHealth();
                var hit = Instantiate(hitSpawn, defender.transform);
                Destroy(hit, 2f);
                var dam = Instantiate(this.damageLabel, defender.transform);
                dam.SetDamage(damage);
                dam.PlayFor(defender.transform);
                yield return MoveCrt.MoveFromTo(transform1, targetPosition, attackerPosition, .3f);
                BattleManager.Instance.NextUnit();
            }
        }

        public void HandleExitState() { }

        public void ProcessChoice(ArmyUnitInstance unitInstance) { }
    }
}