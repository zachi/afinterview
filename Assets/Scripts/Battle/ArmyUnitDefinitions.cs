using System.Collections.Generic;
using AFSInterview.Items.Battle.Presentation;
using UnityEngine;
namespace AFSInterview.Items.Battle
{
    [CreateAssetMenu(fileName = nameof(ArmyUnitDefinitions), menuName = "Battle/" + nameof(ArmyUnitDefinitions), order = 0)]
    public class ArmyUnitDefinitions : ScriptableObject
    {
        [SerializeField] private List<ArmyUnitDefinition> units;
        [SerializeField] private List<ArmyUnitPresenter> prefabs;
        
        public IReadOnlyList<ArmyUnitDefinition> Units => units;
        public IReadOnlyList<ArmyUnitPresenter> Presenters => prefabs;
    }
}