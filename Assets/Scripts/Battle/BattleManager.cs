using System.Collections.Generic;
using System.Linq;
using AFSInterview.Items.Battle.Presentation;
using UnityEngine;
using UnityEngine.Assertions;

namespace AFSInterview.Items.Battle
{
    public sealed class BattleManager : MonoBehaviour
    {
        public static BattleManager Instance { get; private set; }

        [SerializeField] private ArmyUnitDefinitions definitions;

        [SerializeField] private BattleStateMachine stateMachine;
        [SerializeField] private Army leftArmy;
        [SerializeField] private Army rightArmy;

        private readonly Dictionary<ArmyUnitInstance, Army> unitToArmyMap = new();
        private readonly Dictionary<Army, List<ArmyUnitInstance>> armyToUnitsMap = new();
        private readonly Dictionary<ArmyUnitInstance, ArmyUnitPresenter> unitToPresenterMap = new();

        private readonly List<ArmyUnitInstance> units = new();
        private readonly List<ArmyUnitInstance> fightOrder = new();

        private int currentUnitIndex = 0;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            RegisterArmy(leftArmy);
            RegisterArmy(rightArmy);
            BeginBattle();
        }

        private void RegisterArmy(Army army)
        {
            var unitsInCurrentArmy = new List<ArmyUnitInstance>();
            var views = new List<ArmyUnitPresenter>();
            foreach (var unitId in army.Units)
            {
                var definition = definitions.Units.FirstOrDefault(u => u.ID == unitId);
                var viewPrefab = definitions.Presenters.FirstOrDefault(u => u.ForUnitId == unitId);
                Assert.IsNotNull(definition, $"Cannot find definition for {unitId}");
                Assert.IsNotNull(viewPrefab, $"Cannot find presentation prefab for {unitId}");

                var unitInstance = new ArmyUnitInstance(definition);
                var unitPresenter = Instantiate(viewPrefab, army.transform);
                unitPresenter.BindInstance(unitInstance);
                views.Add(unitPresenter);

                units.Add(unitInstance);
                unitToArmyMap.Add(unitInstance, army);
                unitToPresenterMap.Add(unitInstance, unitPresenter);
                unitsInCurrentArmy.Add(unitInstance);
            }
            armyToUnitsMap.Add(army, unitsInCurrentArmy);
            army.UpdateUnitsPlacements(views);
        }

        private void BeginBattle()
        {
            currentUnitIndex = 0;
            CalculateAttacksOrder();
            NextUnit();
        }

        public void NextUnit()
        {
            if (CheckGameOverCondition())
            {
                stateMachine.GameOver();
                Debug.Log("Proper game over");
                return;
            }

            IncreaseIndex();
            while (!stateMachine.EnterStateForUnitAction(fightOrder[currentUnitIndex]))
                IncreaseIndex();
        }

        private void IncreaseIndex()
        {
            currentUnitIndex++;
            if (currentUnitIndex >= fightOrder.Count)
                currentUnitIndex = 0;
        }

        private bool CheckGameOverCondition()
        {
            return IsArmyKilled(leftArmy) || IsArmyKilled(rightArmy);
        }

        private bool IsArmyKilled(Army army) => armyToUnitsMap[army].All(unit => unit.IsAlive() == false);

        private void CalculateAttacksOrder()
        {
            fightOrder.Clear();
            fightOrder.AddRange(units.OrderBy(x => Random.Range(0, 100)));
        }

        public void MarkUnit(ArmyUnitInstance unitInstance, bool mark)
        {
            unitToPresenterMap[unitInstance].MarkActive(mark);
        }

        public void MarkPossibleTargets(ArmyUnitInstance unitInstance, bool mark)
        {
            var unitArmy = unitToArmyMap[unitInstance];
            var otherArmy = unitArmy == leftArmy ? rightArmy : leftArmy;

            foreach (var otherUnit in armyToUnitsMap[otherArmy])
            {
                unitToPresenterMap[otherUnit].IsMarkedAsPossibleTarget = otherUnit.IsAlive() && mark;
            }
        }
        
        public void PlayAttack(ArmyUnitInstance forUnit, ArmyUnitInstance unitInstance, int damage)
        {
            stateMachine.PlayAnimAttack(unitToPresenterMap[forUnit], unitToPresenterMap[unitInstance], damage);
        }
    }
}