using System;
using System.Collections.Generic;
using UnityEngine;

namespace AFSInterview.Items.Battle
{
    [Serializable]
    public class ArmyUnitDefinition
    {
        [SerializeField] private ArmyUnitId id;
        [SerializeField] private ArmyUnitAttributes attributes;
        [SerializeField] private int initialHealthPoints;
        [SerializeField] private int armorPoints;
        [SerializeField] private int attackInterval;
        [SerializeField] private int attackDamage;
        [SerializeField] private AttackDamageOverride[] damageOverrides;

        public ArmyUnitId ID => id;
        public ArmyUnitAttributes Attributes => attributes;
        public int InitialHealthPoints => initialHealthPoints;
        public int ArmorPoints => armorPoints;
        public int AttackInterval => attackInterval;
        public int AttackDamage => attackDamage;
        public IReadOnlyList<AttackDamageOverride> DamageOverrides => damageOverrides;
    }
}