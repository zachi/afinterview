using System;
using UnityEngine;

namespace AFSInterview.Items.Battle
{
    [Serializable]
    public struct AttackDamageOverride
    {
        [SerializeField] private ArmyUnitAttributes attribute;
        [SerializeField] private int overrideValue;

        public ArmyUnitAttributes Attribute => attribute;
        public int OverrideValue => overrideValue;
    }
}