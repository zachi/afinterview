using System;
namespace AFSInterview.Items.Battle
{
    [Flags]
    public enum ArmyUnitAttributes
    {
        None = 0,
        Light = 1,
        Armored = 2,
        Mechanical = 4
    }
}