namespace AFSInterview.Items.Battle
{
    public static class DamageResolver
    {
        public static int CalculateTrueDamage(ArmyUnitDefinition damageGiver, ArmyUnitDefinition damageReceiver)
        {
            int damage = damageGiver.AttackDamage;

            foreach (var overrideOption in damageGiver.DamageOverrides)
            {
                if ((overrideOption.Attribute & damageReceiver.Attributes) != 0)
                    damage = overrideOption.OverrideValue;
            }

            damage = System.Math.Max(damage - damageReceiver.ArmorPoints, 1);
            return damage;
        }
    }
}