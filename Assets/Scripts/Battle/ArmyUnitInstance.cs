using System;
using UnityEngine;

namespace AFSInterview.Items.Battle
{
    [Serializable]
    public class ArmyUnitInstance
    {
        public event Action<int> TurnsToAttackChanged;
        
        [SerializeField] private ArmyUnitDefinition definition;
        [SerializeField] private int turnsToAttack;
        [SerializeField] private int remainingHealth;
        public ArmyUnitDefinition Definition => definition;

        public int TurnsToAttack => turnsToAttack;
        public int RemainingHealth => remainingHealth;

        public ArmyUnitInstance(ArmyUnitDefinition definition)
        {
            this.definition = definition;
            remainingHealth = definition.InitialHealthPoints;
            turnsToAttack = definition.AttackInterval;
        }

        public bool IsAlive() => remainingHealth > 0;
        
        public bool WouldProcessTurn()
        {
            turnsToAttack--;
            TurnsToAttackChanged?.Invoke(turnsToAttack);
            return turnsToAttack <= 0;
        }

        public void ResetTurnsCounter()
        {
            turnsToAttack = definition.AttackInterval;
            TurnsToAttackChanged?.Invoke(turnsToAttack);
        }

        public void TakeDamage(int damage)
        {
            remainingHealth -= damage;
        }
    }
}