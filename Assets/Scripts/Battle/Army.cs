using System.Collections.Generic;
using AFSInterview.Items.Battle.Presentation;
using UnityEngine;

namespace AFSInterview.Items.Battle
{
    public class Army : MonoBehaviour
    {
        [SerializeField] private List<ArmyUnitId> units;
        public IReadOnlyList<ArmyUnitId> Units => units;

        public void UpdateUnitsPlacements(List<ArmyUnitPresenter> myUnits)
        {
            float distance = 10f / myUnits.Count;
            float currX = -5f;
            
            foreach (var armyUnitPresenter in myUnits)
            {
                armyUnitPresenter.transform.localPosition = new Vector3(currX += distance, 0f, 0f);
            }
        }
    }
}