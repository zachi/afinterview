using System.Collections;
using AFSInterview.Quircks;
using TMPro;
using UnityEngine;

namespace AFSInterview.Items.Battle.Presentation
{
    public class DamageLabel : MonoBehaviour
    {
        [SerializeField] private TMP_Text label;

        public void SetDamage(int damage)
        {
            label.text = (-damage).ToString();
        }

        public void PlayFor(Transform t)
        {
            StartCoroutine(Play());

            IEnumerator Play()
            {
                var transform1 = transform;
                var position = t.position;
                transform1.position = position;
                Vector3 targetScale = Vector3.one * -1f;

                yield return MoveCrt.MoveFromTo(transform1, position + Vector3.up, position + Vector3.up * 2f, .7f);

                while (transform.localScale.x > 0f)
                {
                    transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, 4f * Time.deltaTime);
                    yield return null;
                }
                
                Destroy(gameObject);
            }
        }
    }
}