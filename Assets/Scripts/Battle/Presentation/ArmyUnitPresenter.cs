using System.Collections;
using TMPro;
using UnityEngine;

namespace AFSInterview.Items.Battle.Presentation
{
    public class ArmyUnitPresenter : MonoBehaviour
    {
        [SerializeField] private TMP_Text nameLabel;
        [SerializeField] private TMP_Text hitPointsLabel;
        [SerializeField] private TMP_Text turnsToAttackLabel;
        
        [SerializeField] private ArmyUnitId forUnitId;

        private bool isMarkedAsPossibleTarget;
        public ArmyUnitInstance UnitInstance
        {
            get;
            private set;
        }

        [SerializeField] private GameObject _markActive;
        [SerializeField] private GameObject _markPossibleTarget;
        
        public ArmyUnitId ForUnitId => forUnitId;
        public bool IsMarkedAsPossibleTarget
        {
            get => isMarkedAsPossibleTarget;
            set
            {
                isMarkedAsPossibleTarget = value;
                _markPossibleTarget.SetActive(value);
            }
        }

        public void BindInstance(ArmyUnitInstance instance)
        {
            UnitInstance = instance;
            UnitInstance.TurnsToAttackChanged += OnTurnsToAttackChanged;
            
            OnTurnsToAttackChanged(UnitInstance.TurnsToAttack);
        }
        
        private void OnTurnsToAttackChanged(int turns)
        {
            turnsToAttackLabel.text = "Turns to attack: " + turns;
        }

        public void UpdateHealth()
        {
            hitPointsLabel.text = "HP: " + UnitInstance.RemainingHealth;
            if (UnitInstance.RemainingHealth <= 0)
                PlayDead();
        }

        private void OnDestroy()
        {
            if (UnitInstance != null)
            {
                UnitInstance.TurnsToAttackChanged -= OnTurnsToAttackChanged;
            }
        }

        private void Awake()
        {
            nameLabel.text = forUnitId.ToString();
        }

        public void MarkActive(bool active)
        {
            _markActive.SetActive(active);
        }

        private void PlayDead()
        {
            StartCoroutine(PlayDeadCoroutine());
            
            IEnumerator PlayDeadCoroutine()
            {
                var localPosition = transform.localPosition;
                Vector3 targetPosition = new Vector3(localPosition.x, -1f, localPosition.z); 
                
                while (transform.localPosition.y > targetPosition.y)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, 1f * Time.deltaTime);
                    yield return null;
                }
            }
        }
    }
}