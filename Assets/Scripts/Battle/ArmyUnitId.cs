namespace AFSInterview.Items.Battle
{
    public enum ArmyUnitId
    {
        LongSwordKnight,
        Druid,
        Ram,
        Archer,
        Catapult
    }
}